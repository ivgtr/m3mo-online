import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import firebase from 'firebase'
import VeeValidate, { Validator } from 'vee-validate'
import ja from 'vee-validate/dist/locale/ja.js'
import './assets/scss/style.scss'


Vue.config.productionTip = false;
const firebaseConfig = {
  //firebase APIkey
};

firebase.initializeApp(firebaseConfig);
Vue.use(VeeValidate)
Validator.localize('ja', ja)


new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
